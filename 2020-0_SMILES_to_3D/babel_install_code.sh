#!/bin/bash

# babel_install_code.sh
# This script installs Open Babel.
# Edited by Alex Chew (04/15/2020)

# TO RUN:
#	bash babel_install_code.sh

# Reference 1: https://open-babel.readthedocs.io/en/latest/Installation/install.html#compiling-open-babel
# Reference 2: https://openbabel.org/docs/dev/Installation/install.html

# DIRECTORIES #

echo "----- babel_install_code.sh -----"

# Root directory for all installations
bld_dir="${HOME}/${USER}/local_install"

## DEFINING VERSION
version="2.4.1"
# "3.0.0"
# "2.3.2"

## DEFINING PREFIX
prefix="openbabel-${version}"

## DEFINING TAR FILE
tar_file="${prefix}.tar.gz"

## PATH TO BUILD
path_to_build="${bld_dir}/${prefix}"

## MAKING DIRECTORY
mkdir -p "${path_to_build}"
cd "${path_to_build}"


## GETTING FROM ONLINE
wget "https://sourceforge.net/projects/openbabel/files/openbabel/2.4.1/openbabel-2.4.1.tar.gz/download"

## DECOMPRESSING
echo "*** DECOMPRESSING DOWNLOAD FILE ***"
tar -xf "download"

echo "*** BUILDING ***"
## MAKING BUILD DIRECTORY
mkdir build
cd build

## CMAKE TO INSTALL
cmake ../${prefix} -DCMAKE_INSTALL_PREFIX="${path_to_build}"

## MAKING
make -j4 
make install

## PRINTING
echo "Complete!"
echo "---------------------"
echo "Please add the following to your bash profile:"
echo 'export PATH="${HOME}/local_install/openbabel-2.4.1/bin:$PATH"'
echo "---------------------"
echo "Then, load your bash profile again by source ~/.bash_profile"
echo "Test if obabel works: > obabel"

## AFTER INSTALLING, ADD BABEL TO PATH (Add to ~/.bash_profile)
# export PATH="${HOME}/local_install/openbabel-2.4.1/bin:$PATH"
