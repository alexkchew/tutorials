## vmd_plot_STRAP.sh
# The purpose of htis script is to generate polymer images for the STRAP project
#
# Run by: vmd_plot_STRAP

# Written by Alex K. Chew (11/15/2019)
# 
# To generate trajectory: gmx trjconv -f poly_solv_prod.gro -o poly_solv_prod_center.gro -pbc mol -center -s poly_solv_prod.tpr
# To run: play "/Users/alex/tutorials/2020-1_Compute_energy_groups/vmd_plot_STRAP.sh"


## GLOBAL PATHS
## SETTING WORKING DIRECTORY
set working_dir {/Users/alex/tutorials/2020-1_Compute_energy_groups/P_in_S-PET_trimer-acetone-300_K}
# NOTE: Please change "working_dir" to your current directory

### FUNCTIONS

### FUNCTION TO FIX THE DISPLAY
proc fix_display {} {
    # Make background white
    color Display Background white
    
    # Turns off axes location
    axes location Off
    
    # Turns depthcue off
    display depthcue off
    
    # Turns projection to orthographic
    display projection Orthographic 
}

# This function removes all current representations
proc removing_all_rep {} {
    set totalRep [ molinfo top get numreps ]
    
    # Initiating for loop
    for {set x 0} {$x <= $totalRep} {incr x} {	 
    # Deleting top representation
    mol delrep 0 0
    }
}

# Loading molecule, given a path
# Ex: load_molecule_gro {R:\scratch\SideProjectHuber\levoglucosan\mdRun_LGA_70_Percent_water\mixed_solv_frac_04_prod.gro}
proc load_molecule_gro {path} {
    mol new $path
}

#### MAIN KEY

## SETTING GRO FILE
set gro_file "poly_solv_prod_center.gro"

## SETTING PATH
set path_gro "$working_dir/$gro_file"

## DEFINING SOLVENT NAME
set polymer_name "PT3"
set solvent_name "ACE"

## DEFINING MONOMERS
set monomer_1 "1 3 45 2 4 5 6 7 9 12 8 10 11 13 14 15 16 17 19 18 20 21 22 23"
set monomer_2 "23 24 25 26 27 28 29 32 36 33 37 30 34 38 31 35 39 41 40 42 43 44 46"
set monomer_3 "46 47 48 49 50 51 52 54 58 57 61 53 56 60 55 59 62 63 64 65 66 67 68"

## PRINTING
echo "Working on $working_dir"

## FIXING DISPLAY
fix_display

## CLEARING ALL MOLECULES
mol delete all

## LOADING MOLECULE
load_molecule_gro $path_gro

## DELETING ALL REPRESENTATIONS
mol delrep top all

## TURN OFF DEPTH CUE
display depthcue off

## GETTING BOX SIZE
set boxSize [ getSize ]

# GETTING HALF BOX SIZE
set xDim [ lindex $boxSize 0 ]
set xDivided [ expr $xDim/2.0 ]
set centerVector "$xDivided $xDivided $xDivided" 

# POLYMER REPRESENTATION
mol selection "resname $polymer_name and serial $monomer_1"
mol representation CPK 1.000000 0.300000 100.000000 50.000000
mol material Opaque
mol color ColorID 0
mol addrep top

# POLYMER REPRESENTATION
mol selection "resname $polymer_name and serial $monomer_2"
mol representation CPK 1.000000 0.300000 100.000000 50.000000
mol material Opaque
mol color ColorID 2
mol addrep top

# POLYMER REPRESENTATION
mol selection "resname $polymer_name and serial $monomer_3"
mol representation CPK 1.000000 0.300000 100.000000 50.000000
mol material Opaque
mol color ColorID 1
mol addrep top

## GETTING MOLECULE ID
set current_mol_id [ molinfo top ]

## SHOWING SOLVENTS
mol selection "same residue as resname $solvent_name and not name \"H.*\"  and z > $xDivided"
mol representation Licorice 0.10000 100.00000 50.00000
mol material Transparent
mol color Name
mol addrep top

## TURNING ON UPDATE
mol selupdate 3 $current_mol_id 1

## GETTING PBC BOX
pbc box -color black

## ROTATING
#rotate x by 90
rotate y by 180
rotate z by -90

## ZOOMING
scale by 1.200000
scale by 1.200000
scale by 1.200000
