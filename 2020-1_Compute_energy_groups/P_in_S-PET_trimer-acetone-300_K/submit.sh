#!/bin/bash 
# submit_polymer_solvent.sh
# This script is to run polymer in solvent system

## VARIABLES:
#   P_in_S-PET_trimer-acetone-300_K <-- job name
#   28 <-- number of cores
#   equil.mdp <-- input equilibration file
#   prod.mdp <-- input production file
#   poly_solv.top <-- topology file
#   poly_solv <-- output prefix for the job
#   poly_solv_em.gro <-- energy minimizaed gro file
#   10 <-- number of maxwarns for thesystem

###SERVER_SPECIFIC_COMMANDS_START

#SBATCH -p compute
#SBATCH -t 108:00:00
#SBATCH -J P_in_S-PET_trimer-acetone-300_K
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=1              # total number of mpi tasks requested
#SBATCH --mail-user=akchew@wisc.edu
#SBATCH --mail-type=all  # email me when the job starts

## DEFINING NUMBER OF CORES
num_cores="28"

###SERVER_SPECIFIC_COMMANDS_END

# SUBMIT THIS SCRIPT using the command sbatch thisscriptname

## DEFINING PREFIX
output_prefix="poly_solv"

## DEFINING DEFAULT FILES
em_gro_file="poly_solv_em.gro"
top_file="poly_solv.top"

## DEFINING MAX WARN
max_warn="10"

## MDPFILES
equil_mdp_file="equil.mdp"
prod_mdp_file="prod.mdp"

#####################
### EQUILIBRATION ###
#####################
gmx grompp -f ${equil_mdp_file} -c "${em_gro_file}" -p "${top_file}" -o "${output_prefix}_equil" -maxwarn "${max_warn}"

### RUNNING
if [ ! -e "${output_prefix}_equil.gro" ]; then
	## SEEING IF XTC FILE EXISTS
	if [ -e "${output_prefix}_equil.xtc" ]; then
		## RESTARTING
		gmx mdrun -nt "${num_cores}" -v -s "${output_prefix}_equil.tpr" -cpi "${output_prefix}_equil.cpt" -append -deffnm "${output_prefix}_equil"
	else
		## STARTING
		gmx mdrun -nt "${num_cores}" -v -deffnm "${output_prefix}_equil"
	fi
fi

##################
### PRODUCTION ###
##################
gmx grompp -f ${prod_mdp_file} -c "${output_prefix}_equil.gro" -p "${top_file}" -o "${output_prefix}_prod" -maxwarn "${max_warn}"

## SEEING IF GRO FILE EXISTS
if [ ! -e "${output_prefix}_prod.gro" ]; then
	## SEEING IF XTC FILE EXISTS
	if [ -e "${output_prefix}_prod.xtc" ]; then
		## RESTARTING
		gmx mdrun -nt "${num_cores}" -v -s "${output_prefix}_prod.tpr" -cpi "${output_prefix}_prod.cpt" -append -deffnm "${output_prefix}_prod"
	else
		## STARTING
		gmx mdrun -nt "${num_cores}" -v -deffnm "${output_prefix}_prod"
	fi
else
	echo "Job has completed!"
fi


