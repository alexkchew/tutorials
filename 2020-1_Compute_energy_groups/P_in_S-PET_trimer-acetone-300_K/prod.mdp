; prod.mdp
; This script is an MDP file for the AMBER force field. 
; This is a production run with a pressure coupling of Parrinello-Rahman and temperature coupling of velocity rescale.
; Variables to edit:
;	5000000 <-- number of steps in the simulation
; 	300 <-- temperature of the system

; Run control
integrator  = md
tinit       = 0
nsteps      = 5000000 ; 20 ns
dt          = 0.002

; Removal of center of mass motion
comm_mode   = Linear ; Remove center of mass translational velocity
nstcomm     = 10 ; more frequent steps for removal of velocities

; Ouput control
nstxout     = 0
nstvout     = 0
nstfout     = 0
nstxtcout   = 5000 ; 10 ps output
nstenergy   = 500

; General options
continuation = yes
pbc          = xyz
gen-vel      = no	; assign velocities from Maxwell distribution

; Neighbor list options
cutoff-scheme = Verlet
nstlist       = 10		; recommended for verlet algorithm
ns-type       = grid

; Electrostatics
coulombtype      = PME
coulomb-modifier = Potential-shift-Verlet
rcoulomb         = 1.2
fourierspacing   = 0.14
ewald-rtol       = 1e-6
pme-order        = 4

; vdW interactions
vdwtype      = Cut-off
vdw-modifier = Potential-shift-Verlet
rvdw         = 1.2
DispCorr     = No

; Thermostat
tcoupl          = v-rescale
tc-grps         = System
ref-t           = 300
tau-t           = 2.0
nh-chain-length = 1

; Barostat
pcoupl          = Parrinello-Rahman
pcoupltype      = isotropic
ref_p           = 1.0
tau-p           = 5.0 ; 2.0 -- Weakly coupled to prevent system blowup
compressibility = 5e-5 ; 3e-5
